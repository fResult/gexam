package com.g_able.back_g_exam.controller;

import com.g_able.back_g_exam.controller.common.CommonController;
import com.g_able.back_g_exam.entity.Product;
import com.g_able.back_g_exam.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ProductController extends CommonController {

  private final ProductService productService;

  @GetMapping("/products")
  public List<Product> findProducts() {
    return productService.findProducts();
  }

  @GetMapping("/products/{id}")
  public Product findProductById(@PathVariable Integer id) {
    System.out.println("findProductById");
    return null;
  }

  @PostMapping(value = "/products")
  public Product createProduct(@RequestBody Product product) {
    System.out.println("createProduct");
    return null;
  }

  @PutMapping("/products/{id}")
  public Product updateProductById(@PathVariable Integer id, @RequestBody Product product) {
    System.out.println("updateProductById");
    return null;
  }

  @PatchMapping("/products/{id}")
  public Product updatePartialProductById(
      @PathVariable Integer id, @RequestParam("productName") String name, @RequestParam(required = false) String detail,
      @RequestParam(required = false, defaultValue = "0") double price) {
    System.out.println("updatePartialProductById");
    return null;
  }

  @DeleteMapping("/products/{id}")
  public Product deleteProductById(@PathVariable Integer id) {
    System.out.println("deleteProductById");
    return null;
  }

}
