package com.g_able.back_g_exam.controller.advice;

import com.g_able.back_g_exam.exception.CommonException;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@RestControllerAdvice
public class CommonAdvice extends ResponseEntityExceptionHandler {

  @Getter
  @Setter
  private class ExceptionResponse {
    private String code;
    private String message;
    private LocalDateTime timestamp;
  }

  private ResponseEntity<Object> handle(String message, HttpStatus status, String code) {
    ExceptionResponse response = new ExceptionResponse();
    response.setCode("inv-" + code); // inv is a abbreviation of inventory (Service alias)
    response.setMessage(message);
    response.setTimestamp(LocalDateTime.now());

    HttpHeaders headers = new HttpHeaders();
    headers.add("x-developer", "fResult");
    headers.add("x-company", "fResult.dev");
    headers.add("x-info", "https://fResult.dev/api/v1/errors/" + code);

    return ResponseEntity.status(status).headers(headers).body(message);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return handle(ex.getMessage(), status, String.valueOf(status.value()));
  }

  @ExceptionHandler(CommonException.class)
  protected ResponseEntity<Object> handleCommonException(CommonException ex) {
    return handle(ex.getMessage(), ex.getStatus(), ex.getCode());
  }

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleException(CommonException ex) {
    return handle(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
  }

}
