package com.g_able.back_g_exam.exception;

import org.springframework.http.HttpStatus;

public class UnAuthorizeException extends CommonException {

  private final HttpStatus STATUS = HttpStatus.UNAUTHORIZED;
  private final String CODE = "401";

  public UnAuthorizeException(String message) {
    super(message);
  }

}
