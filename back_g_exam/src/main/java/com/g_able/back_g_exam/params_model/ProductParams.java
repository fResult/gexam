package com.g_able.back_g_exam.params_model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder(toBuilder = true)
public class ProductParams {

  private String name;
  private String detail;
  private double minPrice;
  private double maxPrice;

}
