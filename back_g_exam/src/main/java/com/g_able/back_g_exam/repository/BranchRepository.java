package com.g_able.back_g_exam.repository;

import com.g_able.back_g_exam.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {

  List<Branch> findDistinctByOrderByNameAsc();

  Branch findBranchByName(String name);

}
