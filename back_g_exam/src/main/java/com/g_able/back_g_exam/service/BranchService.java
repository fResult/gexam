package com.g_able.back_g_exam.service;

import com.g_able.back_g_exam.entity.Branch;
import com.g_able.back_g_exam.repository.BranchRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class BranchService {
  private final BranchRepository branchRepository;
  public List<Branch> findBranches() {
    return branchRepository.findDistinctByOrderByNameAsc();
  }

  public Branch findBranchByName(String name) {
    return branchRepository.findBranchByName(name);
  }


  public Optional<Branch> findBranchById(Integer branchId) {
    return branchRepository.findById(branchId);
  }

  public Optional<Branch> updateBranch(Integer branchId, Branch branch) {
    Optional<Branch> optBranch = branchRepository.findById(branchId);
    if (!optBranch.isPresent()) return optBranch;

    branch.setId(branchId);
    return Optional.of(branchRepository.save(branch));
  }

}
