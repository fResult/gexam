package com.g_able.back_g_exam.service;

import com.g_able.back_g_exam.entity.Product;
import com.g_able.back_g_exam.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class ProductService {

  private final ProductRepository productRepository;

  public List<Product> findProducts() {
    return productRepository.findDistinctByOrderByNameAsc();
  }

  public Product findProductByName(String name, double price) {
    return productRepository.findProductByNameAndPriceGreaterThanEqual(name, price);
  }

  public Optional<Product> findProductById(Integer productId) {
    return productRepository.findById(productId);
  }

  public Optional<Product> updateProduct(Integer productId, Product product) {
    Optional<Product> optProduct = productRepository.findById(productId);
    if (!optProduct.isPresent()) return optProduct;

    product.setId(productId);
    return Optional.of(productRepository.save(product));
  }

}
