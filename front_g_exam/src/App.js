import React from 'react';
import './App.css';
import InventoryPage from './pages/inventory/InventoryPage';
import { Layout } from 'antd';

const { Header, Footer, Content } = Layout;

function App() {
  return (
    <>
      <Layout>
        <Header className="App-Header">Header</Header>
        <Layout>
          {/*<Sider className="App-Sider">Sider</Sider>*/}
          <Content>
            <InventoryPage />
          </Content>
        </Layout>
        <Footer className="App-Footer">
          Footer
        </Footer>
      </Layout>
    </>
  );
}

export default App;
