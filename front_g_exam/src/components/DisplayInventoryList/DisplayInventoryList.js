import React, { Component } from 'react';
import { Table, Divider, Tag, Button, Empty, Row, Icon } from 'antd';
import { Link } from 'react-router-dom';

import styles from './DisplayInventoryList.module.css';
import ModalUpdateQuantity from 'components/ModalUpdateQuantity/ModalUpdateQuantity';
import { selectStore } from 'services/redux/actions/actionsCreator';
import { connect } from "react-redux";

class DisplayInventoryList extends Component {

  state = {
    filteredInfo: null,
    sortedInfo: null,
    isUpdateQtyModalVisible: false,
  };

  compareByAlph(a, b) {
    if (a > b) return -1;
    if (a < b) return 1;
    return 0;
  }

  handleChange = (pagination, filters, sorter) => {
    console.info('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  handleSelectToUpdateQuantity = (storeId) => {
    const { selectStore, stores } = this.props;
    selectStore(stores, storeId);
    this.handleShowUpdateQtyModal(true);
  };

  handleShowUpdateQtyModal = isUpdateQtyModalVisible => this.setState({ isUpdateQtyModalVisible });

  columns = [
    {
      title: 'Product Name', dataIndex: 'productName', key: 'productName',
      render: text => <Link to="/">{text}</Link>,
      filters: this.props.stores.map(store => ({ text: store.product.name, value: store.product.name })),
      filteredValue: this.state.filteredInfo && (this.state.filteredInfo.productName || null),
      onFilter: (value, record) => record.productName.includes(value),
      sorter: (a, b) => this.compareByAlph(a.productName, b.productName),
    },
    {
      title: 'Price', dataIndex: 'price', key: 'price', align: 'right',
      sorter: (a, b) => a.price - b.price
    },
    {
      title: 'Branch', dataIndex: 'branch', key: 'branch', render: text => <Link to="/">{text}</Link>,
      sorter: (a, b) => this.compareByAlph(a.branch, b.branch)
    },
    {
      title: 'Quantity', dataIndex: 'quantity', key: 'quantity', align: 'right',
      sorter: (a, b) => a.quantity - b.quantity
    },
    {
      title: 'Tag', key: 'tag', dataIndex: 'tag', align: 'center',
      render: tag => (
        <span>
          <Tag color={tag === 'NORMAL' ? '' : tag === 'MORE' ? 'green' : 'volcano'} key={tag}>
            {tag}
          </Tag>
        </span>
      ),
    },
    {
      title: 'Action', key: 'action',
      render: (text, record) => (
        <span>
        <Button className={styles.ButtonEdit} onClick={() => this.handleSelectToUpdateQuantity(record.id)}>
          <Icon type="plus" style={{ fontSize: 12 }} /> / <Icon type="minus" style={{ fontSize: 12 }} /></Button>
        <Divider type="vertical" style={{ background: '#FFB369' }} />
        <Button type="danger" className={styles.ButtonRemove} onClick={() => this.props.onRemoveStore(record.id)}>
          <Icon type="delete" style={{ fontSize: 18 }} />
        </Button>
      </span>
      ),
    },
  ];

  render() {
    const { isUpdateQtyModalVisible } = this.state;
    return (
      <>
        <div style={{ marginBottom: 70 }}>
          <Button onClick={this.props.onFetchStores} type="dashed" style={{ background: '#FFB369' }}>
            Show All Stores
          </Button>

          <ModalUpdateQuantity isUpdateQtyModalVisible={isUpdateQtyModalVisible}
                               onShowUpdateQtyModal={this.handleShowUpdateQtyModal} />

          <Table columns={this.columns} dataSource={this.props.storesTable} onChange={this.handleChange}
                 rowKey={record => record.id}
                 locale={{
                   emptyText: (<Empty
                     image="https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
                     imageStyle={{ height: 60, }}
                     description={<span>Data not found</span>}
                   >
                     <Row className={styles.RowButton}>
                       <Button type="primary" className={styles.ButtonCreate}>Create Product</Button>
                       <Button type="primary" className={styles.ButtonCreate}>Create Branch</Button>
                     </Row>
                     <Row className={styles.RowButton}>
                       <Button type="primary">Add Product in your branch</Button>
                     </Row>
                   </Empty>)
                 }}
          />
        </div>
      </>
    );
  }
}

// const mapStateToProps = (state) => ({
//   store: state.store
// });

const mapDispatchToProps = ({
  selectStore
});


export default connect(null, mapDispatchToProps)(DisplayInventoryList);
