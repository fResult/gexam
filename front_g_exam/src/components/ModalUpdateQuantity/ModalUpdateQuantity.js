import React from 'react';
import { Button, Form, Input, Modal, Select } from 'antd';
import { connect } from 'react-redux';

import axios from 'configs/api.service';
import styles from './ModalUpdateQuantity.module.css';
import { handleError } from 'services/handle-error.service';
import { fetchStores } from 'services/redux/actions/actionsCreator';
import { openUpdateStoreFailedNotification, openUpdateStoreSuccessNotification } from "./ModalUpdateQuantity.noti";

const { Option } = Select;

const ModalUpdateQuantity = props => {

  const { form, store } = props;

  const handleUpdateStores = e => {
    const { form, store, fetchStores } = props;
    e.preventDefault();

    form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        const { operation } = values;
        let quantity = values.quantity;
        if (operation === 'reduce') quantity = -Math.abs(quantity);

        const newStore = { ...store, quantity: store.quantity + quantity };

        try {
          await axios.patch(`/stores/${store.id}`, newStore);
          await fetchStores();
          openUpdateStoreSuccessNotification();
          props.onShowUpdateQtyModal(false);
          form.resetFields();
        } catch (err) {
          const error = handleError(err);
          console.error(err);
          console.error('Error ❌ ', error.message);
          openUpdateStoreFailedNotification(error.message);
        }
      }
    });
  };

  return (
    <>
      <Modal title="Add / Reduce product quantity" style={{ top: 20 }}
             visible={props.isUpdateQtyModalVisible}
             onOk={() => props.onShowUpdateQtyModal(false)}
             onCancel={() => props.onShowUpdateQtyModal(false)}
      >
        <Form
          labelCol={{ xs: 24, sm: 8 }}
          wrapperCol={{ xs: 24, sm: 16 }}
          onClick={handleUpdateStores}
        >
          <Form.Item label="Branch">
            {store.branch && form.getFieldDecorator('branchName', {
              initialValue: store.branch.name
            })(<Input className={styles.InputDisabled} disabled />)}
          </Form.Item>
          <Form.Item label="Product Name">
            {store.product && form.getFieldDecorator('productName', {
              initialValue: store.product.name,
            })(<Input className={styles.InputDisabled} disabled />)}
          </Form.Item>
          <Form.Item label="Operation">
            {form.getFieldDecorator('operation', { initialValue: 'reduce' })(
              <Select>
                <Option value="add" style={{ background: 'dodgerblue' }}>Purchased</Option>
                <Option value="reduce" style={{ background: 'volcano' }} selected>Sold</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="Quantity">
            {form.getFieldDecorator('quantity', {
              getValueFromEvent: (e: React.FormEvent<HTMLInputElement>) => {
                const convertedValue = Number(e.currentTarget.value);
                if (isNaN(convertedValue)) {
                  return Number(props.form.getFieldValue("quantity"));
                } else {
                  return convertedValue;
                }
              },
              rules: [{
                required: true,
                type: 'number'
              }]
            })(<Input />)}
          </Form.Item>
          <Form.Item wrapperCol={{ xs: 24, sm: { span: 16, offset: 8 } }}>
            <Button type="primary" htmlType="submit">
              Update Product Quantity
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

const mapStateToProps = state => ({
  store: state.store
});

const ModalUpdateQuantityForm = Form.create()(ModalUpdateQuantity);
export default connect(mapStateToProps, { fetchStores })(ModalUpdateQuantityForm);
