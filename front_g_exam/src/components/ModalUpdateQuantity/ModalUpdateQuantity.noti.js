import React from "react";
import { notification, Icon } from 'antd';

export const openUpdateStoreSuccessNotification = () => {
  notification.open({
    message: 'Update store successfully',
    description: 'Update success',
    icon: <Icon type="check-circle" style={{ color: 'dodgerblue', fontSize: 24 }} />,
  });
};

export const openUpdateStoreFailedNotification = (errorMessage) => {
  notification.open({
    message: 'Update store failed',
    description: errorMessage,
    icon: <Icon type="close-circle" style={{ color: 'crimson', fontSize: 24 }} />,
  });
};
