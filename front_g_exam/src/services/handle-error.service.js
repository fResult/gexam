export const handleError = (err) => {
  if (!err.response) {
    return { message: err.message };
  }
  return { message: err.response.data };
};
