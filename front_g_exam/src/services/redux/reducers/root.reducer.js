import { combineReducers } from 'redux';
import storesReducer from './stores.reducer';
import storeReducer from "./store.reducer";

const rootReducer = combineReducers({
  stores: storesReducer,
  store: storeReducer
});

export default rootReducer;
