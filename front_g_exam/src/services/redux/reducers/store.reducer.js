import { storeTypes } from 'services/redux/actions/actionTypes';

const initialState = { store: {} };

const storeReducer = (state = initialState, action) => {
  switch (action.type) {
    case storeTypes.SELECT_STORE: {
      return action.stores.filter(store => store.id === action.storeId)[0]
    }
    default:
      return state;
  }
};

export default storeReducer;
