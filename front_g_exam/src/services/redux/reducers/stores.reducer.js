import { storesTypes } from 'services/redux/actions/actionTypes';

const initialState = {
  stores: []
};

const storesReducer = (state = initialState, action) => {
  switch (action.type) {
    case storesTypes.FETCH_STORES: {
      return action.stores
    }
    case storesTypes.SEARCH_STORES_QUICK: {
      return action.stores
    }
    case storesTypes.SEARCH_STORES_ADVANCE: {
      return action.stores
    }
    default:
      return state;
  }
};

export default storesReducer;
